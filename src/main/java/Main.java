import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarStyle;

import javax.swing.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException, NumberFormatException {
        final Scanner SCANNER = new Scanner(System.in);

        // Get # of triples to generate from user
        int tripleCount = Integer.parseInt(JOptionPane.showInputDialog(null, "Enter number of triples to generate.\nCAUTION: Large values may deprive the JVM of memory!"));

        // Get current system time for compute time calculation
        long startTime = System.currentTimeMillis();

        // Find triples
        System.out.println("\nStarting calculation");
        ArrayList<Integer[]> triples = PythagoreanCalc.calculateTriples(tripleCount);
        System.out.println(String.format("Done! Calculated %d triples.", triples.size()));

        // Create file
        File csv = new File("triples.csv");
        csv.createNewFile();

        // Open stream
        FileWriter writer = new FileWriter(csv);

        // Write columns to csv
        writer.write("a,b,c,has_prime,primes,nonprimes,probability\n");

        // Flush data to csv
        ProgressBar progress = new ProgressBar("Data dump", triples.size(), ProgressBarStyle.ASCII);
        progress.start();
        progress.setExtraMessage("Flushing...");
        for( Integer[] triple : triples ) {
            // Step progress bar
            progress.step();
            // Write current line to csv
            writer.write(String.format("%d,%d,%d,%b\n", triple[0], triple[1], triple[2], PythagoreanCalc.containsPrime(triple)));
        }
        progress.stop();
        writer.close();

        System.out.println(String.format("Done! Data written to %s. Press enter to exit.", csv.getAbsolutePath()));
        SCANNER.nextLine();
        System.exit(0);
    }
}
