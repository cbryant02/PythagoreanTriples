import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarStyle;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class PythagoreanCalc {
    /**
     * Calculates n Pythagorean triples, complete with a Swing progress bar.
     *
     * @param limit Upper bound for number of triples
     * @return ArrayList of Integer triplet arrays
     */
    public static ArrayList<Integer[]> calculateTriples(int limit) {
        ArrayList<Integer[]> triples = new ArrayList<>();
        ProgressBar progress = new ProgressBar("Triples", limit, ProgressBarStyle.ASCII);

        int a, b, c;
        int m = 2;

        progress.start();
        try {
            TimeUnit.MILLISECONDS.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        while (triples.size() < limit) {
            for (int n = 1; n < m; n++) {
                a = m * m - n * n;
                b = 2 * m * n;
                c = m * m + n * n;

                if (triples.size() >= limit)
                    break;

                triples.add(new Integer[]{a, b, c});
            }
            m++;

            progress.setExtraMessage("Calculating...");
            progress.stepTo(triples.size());
            try {
                TimeUnit.MILLISECONDS.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        progress.stop();
        return triples;
    }

    public static boolean containsPrime(Integer[] triple) {
        boolean[] prime = new boolean[3];

        for (Integer n : triple) {
            int index = 0;
            for (int i = 2; i < (n/2); i++) {
                if (n%i == 0) {
                    prime[index] = false;
                    break;
                }
                prime[index] = true;
            }
            index++;
        }

        return (prime[0] || prime[1] || prime[2]);
    }
}